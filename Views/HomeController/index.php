<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="../CSSs/homecss.css" />
    <link rel="Stylesheet" type="text/css" href="../CSSs/background.css" />
    <title>Home</title>
</head>
<body>

<div class="container">

    <div class="top">
        <img src="../img/top.svg">
        <div class="loggeduser">
                <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                    if($message == 'Zostałeś wylogowany.') {
                        echo "        <div class=\"btn-z\">
                         <a href=\"?page=logging\"> <img src=\"../img/zalSie.svg\"/></a>
                         </div>
                         
                         <div class=\"btn-p\">
                         <a href=\"?page=logging\"> <img src=\"../img/human2.svg\"/></a>
                         </div>
                         
                         ";
                    }
                    else {

                        echo "        <div class=\"btn-z\">
                        <a href=\"?page=logout\"> <img src=\"../img/wylogSie.svg\"/></a>
                         </div>
                         
                         <div class=\"btn-p\">
                         <a href=\"?page=reservation\"> <img src=\"../img/human2.svg\"/></a>
                         </div>
                         
                         ";
                    }
                }
                else{
                    echo "<div class=\"btn-z\">
                        <a href=\"?page=logging\"> <img src=\"../img/zalSie.svg.svg\"/></a>
                         </div>
                         <div class=\"btn-p\">
                         <a href=\"?page=logging\"> <img src=\"../img /human2.svg\"/></a>
                         </div>
                         
                         ";
                }
                ?>
        </div>

    </div>

    <div class="strip">
        <img src="../img/zdj.svg">
    </div>

    <div class="menu">
        <div class="choice">
            <?php
                if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
                     echo "<a href=\"?page=logging\"> <img src=\"../img/zarTur.svg\"/></a>";
                }
                else{
                    echo "<a href=\"?page=search\"> <img src=\"../img/zarTur.svg\"/></a>";
                }
            ?>
        </div>
        <div class="choice">
            <img src="../img/aktual.svg">
        </div>
        <div class="choice">
            <img src="../img/info.svg">
        </div>
    </div>

</div>
</body>
</html>