<?php
if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
    die('Musisz się zalogować');
}

?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="../CSSs/applicationcss.css" />
    <link rel="Stylesheet" type="text/css" href="../CSSs/background.css" />
    <script src="../JS/script.js"></script>
    <title>Formularz zgłoszeniowy</title>
</head>
<body>
<div class="container">

    <div class="top">
        <img src="../img/top.svg">
        <div class="loggeduser">
            <?php
            if(isset($messages)) {
                foreach ($messages as $message) {
                    echo $message;
                }
                echo "<div class=\"btn-z\">
                      <a href=\"?page=logout\"> <img src=\"../img/wylogSie.svg\"/></a>
                      </div>
                       <div class=\"btn-p\">
                         <a href=\"?page=reservation\"> <img src=\"../img/human2.svg\"/></a>
                         </div>
                      ";
            }
            ?>
        </div>
    </div>
    <div class="register-view">
        <?php
        /*
        if(isset($results)) {
            echo $results;
        }
        */
        ?>
        <img src="../img/daneuczestika.svg">
        <form method="post">
            <input name="name" type="text" placeholder="Imię">
            <input name="surname" type="text" placeholder="Nazwisko">
            <input name="pesel" type="text" placeholder="Pesel">
            <input name="birthdate" type="date" placeholder="Data urodzenia">
            <input name="number" type="text" placeholder="Numer telefonu opiekuna">

            <img src="../img/adres.svg">
            <input name="street" type="text" placeholder="Ulica">
            <input name="code" type="text" placeholder="Kod pocztowy">
            <input name="city" type="text" placeholder="Miejscowość">

            <!div class="checking">
                <!input name="chceck" type="checkbox">

            <!/div>
            <!div class="checking">
                <!input name="chceck" type="checkbox">
            <!/div>

            <?php
            if(isset($slot))
                {
                    if($slot)
                        echo "<button type=\"submit\" onclick=\"sure()\">CONTINUE</button>";
                    else
                        echo "<a>BRAK MIEJSC</a>";
                }
            ?>

        </form>

    </div>


</div>
</body>
</html>