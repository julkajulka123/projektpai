<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="../CSSs/background.css" />
    <link rel="Stylesheet" type="text/css" href="../CSSs/loggingcss.css" />
    <title>Logowanie</title>
</head>
<body>
<div class="container">

    <div class="top">
        <img src="../img/top.svg">

    </div>

    <div class="logging-img">
        <img src="../img/human.svg">
    </div>

    <div class="logging-view">
        <form action="?page=logging" method="POST">
            <div class="messages">
                <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
                ?>
            </div>
            <input name="email" type="text" placeholder="Adres e-mail">
            <input name="password" type="password" placeholder="Hasło">
            <button type="submit">CONTINUE</button>
        </form>

        <div class="text">
            <img src="../img/nmk.svg">
            <a href="?page=registration"> <img src="../img/zarSie.svg"/></a>
        </div>
    </div>


</div>
</body>
</html>