<?php
if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
    die('Musisz się zalogować');
}

?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="../CSSs/background.css" />
    <link rel="Stylesheet" type="text/css" href="../CSSs/searchcss.css" />
    <title>Wyszukiwanie</title>
</head>
<body>
<div class="container">

    <div class="top">
        <img src="../img/top.svg">
        <div class="loggeduser">
            <?php
            if(isset($messages)) {
                foreach ($messages as $message) {
                    echo $message;
                }
                echo "<div class=\"btn-z\">
                      <a href=\"?page=logout\"> <img src=\"../img/wylogSie.svg\"/></a>
                      </div>
                       <div class=\"btn-p\">
                         <a href=\"?page=reservation\"> <img src=\"../img/human2.svg\"/></a>
                         </div>
                      ";
            }
            ?>
        </div>
    </div>

    <div class="resutsbox">

            <?php
            if(isset($results)){
                $c = 0;
                echo "<ul>";
                foreach($results as $current) {
                    $resort= $current->getResort()->getName();
                    $type = $current->getType();
                    $date = $current->getDate();
                    $slotsLeft =  $current->getSlotsLeft();
                    $idStay = $current->getIdStay();
                    echo "<div class=\"result\">
                           <div class='list'>Miejsce: $resort</div>
                           <div class='list'>Typ: $type</div>
                           <div class='list'>Data: $date</div>
                           <div class='list'>Dostępne miejsca: $slotsLeft</div>";
                            if($slotsLeft>0){
                                echo "     <a href=\"?page=application&ids=$idStay\" type=\"submit\" name=\"id\" value=\"$idStay\"> <img src=\"../img/wyslijzgl.svg\"/></a>
                                </div>";
                           }
                           else{
                               echo "<div class='list'>BRAK MIEJSC</div>";
                           }
                    $c += 1;
                }
                echo "</ul>";
            }

            ?>


    </div>

</div>
</body>
</html>