<?php
if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
    die('You are not logged in!');
}

if(!($_SESSION['role']==="ROLE_ADMIN")) {
    die('You do not have permission to watch this page!');
}
?>


<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="Stylesheet" type="text/css" href="../CSSs/usersbck.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <script src="../JS/script.js"></script>
    <title>Admin panel</title>
</head>
<body>
<?php include(dirname(__DIR__).'/Common/navbar.php'); ?>
<div class="container">
    <div class="col-6">
        <table class="table mt-4 text-light">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Email</th>
                <th scope="col">Name</th>
                <th scope="col">Surname</th>
            </tr>
            </thead>
            <tbody class="users-list">
            <?php foreach($users as $user): ?>
                <tr>
                    <th scope="row"><?= $user->getIdUser(); ?></th>
                    <td><?= $user->getEmail(); ?></td>
                    <td><?= $user->getName(); ?></td>
                    <td><?= $user->getSurname(); ?></td>
                </tr>
            <?php endforeach ?>
            </tbody>
            <tbody class="users-list">
            </tbody>
        </table>
        <button class="btn-primary btn-lg ml-0" type="button" onclick="getUsers()">
            Get all users
        </button>
    </div>
</div>
</body>
</html>
