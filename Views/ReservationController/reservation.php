<?php
if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
    die('Musisz się zalogować');
}

?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="../CSSs/background.css" />
    <link rel="Stylesheet" type="text/css" href="../CSSs/searchcss.css" />
    <title>Rezerwacje</title>
</head>
<body>
<div class="container">

    <div class="top">
        <img src="../img/top.svg">
        <div class="loggeduser">
            <?php
            if(isset($messages)) {
                foreach ($messages as $message) {
                    echo $message;
                }
                echo "<div class=\"btn-z\">
                      <a href=\"?page=logout\"> <img src=\"../img/wylogSie.svg\"/></a>
                      </div>
                       <div class=\"btn-p\">
                         <a href=\"?page=reservation\"> <img src=\"../img/human2.svg\"/></a>
                         </div>
                      ";
            }
            ?>
        </div>
    </div>

    <div class="resutsbox">



        <a>Twoje rezerwacje: </a>
        <?php
        if(isset($results)){
            $c = 0;
            echo "<ul>";
            foreach($results as $current) {
                $name = $current->getName();
                $surname = $current->getSurname();
                $date = $current->getDate();
                $resort =  $current->getResort();
                echo "<div class=\"result\">
                           <div class='list'>Uczestnik: $name $surname</div>
                           <div class='list'>Miejsce: $resort</div>
                           <div class='list'>Data: $date</div>
                           </div>"
                ;
                $c += 1;
            }
            echo "</ul>";
        }

        ?>


    </div>

</div>
</body>
</html>