<?php

require_once 'Controllers\HomeController.php';
require_once 'Controllers\LoggingController.php';
require_once 'Controllers\ApplicationController.php';
require_once 'Controllers\RegistrationController.php';
require_once 'Controllers//SearchController.php';
require_once 'Controllers//AdminController.php';
require_once 'Controllers//ReservationController.php';

class Routing {
    private $routes = [];

    public function __construct()
    {
        $this->routes = [
            'reservation' => [
                'controller' => 'ReservationController',
                'action' => 'reservation'
            ],
            'index' => [
                'controller' => 'HomeController',
                'action' => 'home'
            ],
            'logout' => [
                'controller' => 'HomeController',
                'action' => 'logout'
            ],
            'logging' => [
                'controller' => 'LoggingController',
                'action' => 'login'
            ],
            'application' => [
                'controller' => 'ApplicationController',
                'action' => 'application'
            ],
            'registration' => [
                'controller' => 'RegistrationController',
                'action' => 'registration'
            ],
            'search' => [
                'controller' => 'SearchController',
                'action' => 'search'
            ],
            'admin' => [
                'controller' => 'AdminController',
                'action' => 'admin'
            ],
            'users'=> [
                'controller'=> 'AdminController',
                'action'=> 'users']

        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 'index';

        if (isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();
        }
    }
}