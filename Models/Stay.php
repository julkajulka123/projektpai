<?php
require_once 'Resort.php';
require_once 'Address.php';


class Stay {
    private $idStay;
    private $type;
    private $date;
    private $slotsLeft;
    private $resort;


    public function __construct(
        int $idStay,
        string $type,
        string $date,
        Resort $resort,
        int $slotsLeft)
    {
        $this->idStay = $idStay;
        $this->type = $type;
        $this->date = $date;
        $this->resort = $resort;
        $this->slotsLeft = $slotsLeft;
    }

    public function getIdStay(): int
    {
        return $this->idStay;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getSlotsLeft(): int
    {
        return $this->slotsLeft;
    }

    public function getResort(): Resort
    {
        return $this->resort;
    }


}