<?php

class ReservationInfo {
    private $name;
    private $surname;
    private $date;
    private $resort;


    public function __construct(

        string $name,
        string $surname,
        string $date,
        string $resort
    ) {
        $this->name = $name;
        $this->surname = $surname;
        $this->date = $date;
        $this->resort = $resort;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }


    public function getDate(): string
    {
        return $this->date;
    }

    public function getResort(): string
    {
        return $this->resort;
    }


}