<?php

class Vacation {
    private $idVacation;
    private $idStay;
    private $idParticipant;
    private $role = ['ROLE_VACATION'];

    public function __construct($idVacation, $idStay, $idParticipant)
    {
        $this->idVacation = $idVacation;
        $this->idStay = $idStay;
        $this->idParticipant = $idParticipant;
    }

    public function getIdVacation()
    {
        return $this->idVacation;
    }

    public function setIdVacation($idVacation)
    {
        $this->idVacation = $idVacation;
    }

    public function getIdStay()
    {
        return $this->idStay;
    }

    public function setIdStay($idStay)
    {
        $this->idStay = $idStay;
    }

    public function getIdParticipant()
    {
        return $this->idParticipant;
    }

    public function setIdParticipant($idParticipant)
    {
        $this->idParticipant = $idParticipant;
    }


    public function getRole(): array
    {
        return $this->role;
    }


}