<?php
require_once 'Address.php';

class Participant {
    private $idParticipant;
    private $name;
    private $surname;
    private $address;
    private $pesel;
    private $birthdate;
    private $phone;

    public function __construct(
        int $idParticipant,
        string $name,
        string $surname,
        Address $address,
        string $pesel,
        string $birthdate,
        string $phone)
    {
        $this->idParticipant = $idParticipant;
        $this->name = $name;
        $this->surname = $surname;
        $this->address = $address;
        $this->pesel = $pesel;
        $this->birthdate = $birthdate;
        $this->phone = $phone;
    }

    public function getIdParticipant(): int
    {
        return $this->idParticipant;
    }

    public function getName(): string
    {
        return $this->name;
    }


    public function getSurname(): string
    {
        return $this->surname;
    }


    public function getAddress(): Address
    {
        return $this->address;
    }


    public function getPesel(): string
    {
        return $this->pesel;
    }

    public function getBirthdate(): string
    {
        return $this->birthdate;
    }


    public function getPhone(): string
    {
        return $this->phone;
    }



}