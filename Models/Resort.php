
<?php
require_once 'Address.php';

class Resort {
    private $idResort;
    private $slots;
    private $address;
    private $name;

    public function __construct(
        int $idResort,
        int $slots,
        string $name,
        Address $address)
    {
        $this->idResort = $idResort;
        $this->slots = $slots;
        $this->name= $name;
        $this->address = $address;
    }


    public function getSlots(): int
    {
        return $this->slots;
    }

    public function getLocation(): string
    {
        return $this->address->getCity();
    }

    public function getStreet(): string
    {
        return $this->address->getStreet();
    }

    public function getCode(): string
    {
        return $this->address->getCode();
    }

    public function getIdResort(): int
    {
        return $this->idResort;
    }

    public function getName(): string
    {
        return $this->name;
    }

}