function getUsers() {
    const apiUrl = "localhost";
    const $list = $('.users-list');

    $.ajax({
        url : apiUrl + '/?page=users',
        dataType : 'json'
    })
        .done((res) => {
            $list.empty();

            res.forEach(el => {
                $list.append(`<tr>
                        <td>${el.id}</td>
                        <td>${el.email}</td>
                        <td>${el.name}</td>
                        <td>${el.surname}</td>
                        </tr>`);
            });
        });
}

function sure() {
    if(!confirm('Na pewno chcesz się zapisać?')) {
        return false;
    }
}