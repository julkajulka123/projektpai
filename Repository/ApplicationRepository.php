<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//Participant.php';
require_once __DIR__.'//..//Models//Address.php';
require_once __DIR__.'//..//Models//Stay.php';

class ApplicationRepository extends Repository {

    public function signParticipant(string $name, string $surname, string $city, string $street, string $code , string $pesel, string $birthdate, string $number, int $idStay, int $idUser)
    {


        $stmt = $this->database->connect()->prepare("
            START TRANSACTION;
INSERT INTO address(city,street,code) 
	VALUES ('$city','$street','$code');
    
INSERT INTO participant(name,surname,pesel,birthdate,phone,id_user) 
	VALUES ('$name','$surname','$pesel','$birthdate','$number','$idUser');
    
UPDATE participant
SET participant.id_address = (
    select max(address.id_address)
    from address)
WHERE participant.id_address = 0;

INSERT INTO vacation(id_stay,id_participant) VALUES ('$idStay',(select max(participant.id_participant)
    from participant));
    
UPDATE stay
SET stay.slots_left=GREATEST(0, stay.slots_left-1)
WHERE stay.id_stay = '$idStay';

COMMIT;
        ");
        $stmt->execute();

    }

    function available(int $idStay): bool {
        $stmt = $this->database->connect()->prepare("
            SELECT slots_left FROM stay WHERE id_stay='$idStay'
        ");
        $stmt->execute();
        $slot = $stmt->fetch(PDO::FETCH_ASSOC);


        if($slot['slots_left']>0)
            return true;
        else
            return false;
    }

}