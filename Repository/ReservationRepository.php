<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//Resort.php';
require_once __DIR__.'//..//Models//Address.php';
require_once __DIR__.'//..//Models//Stay.php';
require_once __DIR__.'//..//Models//ReservationInfo.php';

class ReservationRepository extends Repository {

    function getUsersReservations(int $id_user): array {
        $result = [];
        $stmt = $this->database->connect()->prepare("
            SELECT p.name, p.surname, s.date, r.name as resort FROM participant p, stay s, resort r, vacation v WHERE s.id_resort=r.id_resort AND v.id_participant = p.id_participant AND v.id_stay=s.id_stay AND p.id_user= '$id_user'");
        $stmt->execute();
        $resorts = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resorts as $resort) {
            $result[] = new ReservationInfo($resort['name'],$resort['surname'],$resort['date'],$resort['resort']);
        }

        return $result;
    }


}