<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//Resort.php';
require_once __DIR__.'//..//Models//Address.php';
require_once __DIR__.'//..//Models//Stay.php';
require_once __DIR__.'//..//Models//ReservationInfo.php';

class UserMapper extends Repository {

    public function getUsers2()
    {

        try{
            $stmt = $this->database->connect()->prepare('SELECT * FROM users WHERE id_user !=:email;');
            $stmt->bindParam(':email', $_SESSION['id'], PDO::PARAM_STR);
            $stmt->execute();
            $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $user;
        }
        catch(PDOException $e) {die();}
    }

    public function getUsers()
    {

        try{
            $stmt = $this->database->connect()->prepare('SELECT * FROM users WHERE id_user =1;');
            $stmt->bindParam(':email', $_SESSION['id'], PDO::PARAM_STR);
            $stmt->execute();
            $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $user;
        }
        catch(PDOException $e) {die();}
    }


}