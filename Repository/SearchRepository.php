<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//Resort.php';
require_once __DIR__.'//..//Models//Address.php';
require_once __DIR__.'//..//Models//Stay.php';

class SearchRepository extends Repository {

  function getResortsName(): array {
      $result = [];
      $stmt = $this->database->connect()->prepare('
            SELECT * FROM resort
        ');
      $stmt->execute();
      $resorts = $stmt->fetchAll(PDO::FETCH_ASSOC);

      foreach ($resorts as $resort) {
          $result[] = $resort['name'];
      }

      return $result;
  }

    function getResorts(): array {
        $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT r.id_resort, a.id_address, a.city, a.street, a.code, r.slots, r.name FROM resort r INNER JOIN address a ON a.id_address=r.id_address 
        ');
        $stmt->execute();
        $resorts = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resorts as $resort) {
            $result[] = new Resort($resort['id_resort'],$resort['slots'],$resort['name'],
                new Address($resort['id_address'],$resort['code'],$resort['city'],$resort['street']));
        }

        return $result;
    }

    function getStays(): array {
        $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT s.id_stay, r.id_resort, a.id_address, a.city, a.street, a.code, r.slots, r.name, s.type, s.date, s.slots_left
            FROM resort r, stay s, address a WHERE s.id_resort=r.id_resort AND a.id_address=r.id_address
        ');
        $stmt->execute();
        $stays = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($stays as $stay) {
            $result[] = new Stay($stay['id_stay'],$stay['type'],$stay['date'],
                new Resort($stay['id_resort'],$stay['slots'],$stay['name'],
                new Address($stay['id_address'],$stay['code'],$stay['city'],$stay['street'])),
                $stay['slots_left']
            )
            ;
        }

        return $result;
    }

}