<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class RegistrationController extends AppController {

    public function registration()
    {
        $userRepository = new UserRepository();

        if ($this->isPost()) {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $name = $_POST['name'];
            $surname = $_POST['surname'];

            $userRepository->addUser($email,$name,$surname,$password);

            $user = $userRepository->getUser($email);

            $_SESSION["id"] = $user->getIdUser();
            $_SESSION["role"] = $user->getRole();


            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=index");
        }

        $this->render('registration');
    }
}