<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//Resort.php';
require_once __DIR__.'//..//Repository//SearchRepository.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class SearchController extends AppController {

    public function search()
    {
        $userRepository = new UserRepository();
        if (isset($_SESSION["id"])) {
            //zalogowany user
            $loggedID = $_SESSION["id"];
            $name = $userRepository->loggedUser($loggedID)->getName();

            $searchRespository = new SearchRepository();
            $stays = $searchRespository->getStays();
            $this->render('search', ['messages' => ['Zalogowany jako '.$name],'results' =>$searchRespository->getStays()]);
            return;
        }


        $this->render('search');

    }
}