<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';
require_once __DIR__.'//..//Repository//UserMapper.php';

class AdminController extends AppController {

    public function admin()
    {
        $userRepository = new UserRepository();
        $users = $userRepository->getUsers();

        $this->render('users', ['users' => $users]);
    }

    public function users()
    {
        $user = new UserMapper();
        header('Content-type: application/json');
        http_response_code(200);
        echo $user->getUsers() ? json_encode($user->getUsers()) : '';
    }
}