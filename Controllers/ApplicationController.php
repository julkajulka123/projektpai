<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Repository//UserRepository.php';
require_once __DIR__.'//..//Repository//ApplicationRepository.php';

class ApplicationController extends AppController {

    public function application()
    {
        $userRepository = new UserRepository();
        $appRepository = new ApplicationRepository();
        if (isset($_SESSION["id"])) {
            //zalogowany user
            $loggedID = $_SESSION["id"];
            $loggedname = $userRepository->loggedUser($loggedID)->getName();
            $idStay = $_GET['ids'];

            if ($this->isPost()) {
                $name = $_POST['name'];
                $surname = $_POST['surname'];
                $city = $_POST['city'];
                $street = $_POST['street'];
                $code = $_POST['code'];
                $pesel = $_POST['pesel'];
                $birthdate = $_POST['birthdate'];
                $number = $_POST['number'];

                $appRepository->signParticipant($name,$surname,$city,$street,$code,$pesel,$birthdate,$number,$idStay,$loggedID);

                $url = "http://$_SERVER[HTTP_HOST]/";
                header("Location: {$url}?page=reservation");
            }



            $this->render('application', ['messages' => ['Zalogowany jako '.$loggedname],'slot' => $appRepository->available($idStay)]);

        }
        $this->render('application');
    }
}