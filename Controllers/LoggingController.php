<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class LoggingController extends AppController {

    public function login()
    {
        $userRepository = new UserRepository();

        if ($this->isPost()) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $user = $userRepository->getUser($email);

            if (!$user) {
                $this->render('logging', ['messages' => ['User with this email not exist!']]);
                return;
            }

            if ($user->getPassword() !== $password) {
                $this->render('logging', ['messages' => ['Wrong password!']]);
                return;
            }

            $_SESSION["id"] = $user->getIdUser();
            $_SESSION["role"] = $user->getRole();
            $_SESSION["name"] = $user->getName();

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=search");
            return;
        }

        $this->render('logging');
    }

}