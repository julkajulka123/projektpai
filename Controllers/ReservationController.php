<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Models//ReservationInfo.php';
require_once __DIR__.'//..//Repository//UserRepository.php';
require_once __DIR__.'//..//Repository//ReservationRepository.php';

class ReservationController extends AppController {

    public function reservation()
    {

        $userRepository = new UserRepository();
        if (isset($_SESSION["id"])) {
            //zalogowany user
            $loggedID = $_SESSION["id"];
            $name = $userRepository->loggedUser($loggedID)->getName();

            $reservationRepository = new ReservationRepository();
            $reservations = $reservationRepository->getUsersReservations($loggedID);

            $this->render('reservation', ['messages' => ['Zalogowany jako '.$name],'results' =>$reservations]);
            return;
        }

        $this->render('reservation');
    }

}